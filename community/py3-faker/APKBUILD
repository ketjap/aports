# Contributor: Drew DeVault <sir@cmpwn.com>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-faker
_pyname=Faker
pkgver=9.8.1
pkgrel=0
pkgdesc="Python package that generates fake data for you"
url="https://faker.readthedocs.io/en/master"
arch="noarch"
license="MIT"
depends="py3-dateutil py3-six py3-text-unidecode"
makedepends="py3-setuptools"
checkdepends="py3-email-validator py3-ipaddress py3-mock py3-freezegun
	py3-more-itertools py3-pytest py3-ukpostcodeparser py3-validators
	py3-pytest-runner py3-random2 py3-pillow"
_pypiprefix="${_pyname%${_pyname#?}}"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir=$srcdir/$_pyname-$pkgver

replaces="py-faker" # Backwards compatibility
provides="py-faker=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# tests erroneously require a specific version of pytest
	sed -i setup.py -e 's/ *"pytest>=.*//g'
}

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
a93d625311358100c7a1dfe33dfda64b5890dd25e8d6e3dd095329ab2156c76abf3f00fe6b2c335316bbe3f42322adda2bcae3a9a312ddf4778d8143796667a0  Faker-9.8.1.tar.gz
"
